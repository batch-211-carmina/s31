// 1. What directive is used by Node.js in loading the modules it needs?
	//answer: require directive
	require()

// 2. What Node.js module contains a method for server creation?
	//answer: http module
	http module

// 3. What is the method of the http object responsible for creating a server using Node.js?
	//answer: createServer() method
	createServer()

// 4. What method of the response object allows us to set status codes and content types?
	//answer: response.end()
	writeHead()

// 5. Where will console.log() output its contents when run in Node.js?
	//answer: in the terminal or browser
	terminal

// 6. What property of the request object contains the address's endpoint?
	//answer: HTTP request
	url
